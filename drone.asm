// 10 SYS (4096):REM by james o'hara

*=$0401

        .byte    $20, $04, $0A, $00, $9E, $20, $28,  $34, $30, $39, $36, $29, $3a, $8f, $20, $42, $59, $20, $4A, $41, $4D, $45, $53, $20, $4F, $27, $48, $41, $52, $41, $00, $00, $00

*=$1000
.var screen = $8000
.var chrin = $ffe4
.var chrout = $ffd2



start:

        jsr drawscreen //copy the backcround to screen mem
        jsr waitforspace // wait for any key to be pressed
        lda #0
        sta dronex
        jsr mainloop
        rts

mainloop:
        jsr drawbolt // draw the lightning bolt
        jsr bolt // flash the screen
        jsr removebolt // remove the lightning bolt
        jsr drawdrone//
loop2:  jsr waitforkey // wait for a keypress
        cmp #4 // is it runstop?
        beq !end+ // if it is then end
        cmp #$38 // is W pressed
        beq moveright // move drone right if it is
        cmp #$40 // is Q pressed
        beq moveleft
        cmp #$2c // is L pressed
        beq mainloop // flash the lightning
        
        jmp loop2
moveright:
        jsr delay
        lda dronex
        cmp #37
        beq continueloop
        jsr removedrone
        inc dronex
        jsr drawdrone
        jmp continueloop
moveleft:
        jsr delay
        lda dronex
        cmp #$00
        beq continueloop
        jsr removedrone
        dec dronex
        jsr drawdrone
        jmp continueloop
continueloop:
        jmp loop2

!end:
        jsr clearscreen
        rts // end the program
bolt:
        ldx #$00
screenloop: // go through the 4 banks of 255 screen mem
        lda screen,x
        eor #$80 // reverses/unreverses the screen character
        sta screen,x
        lda screen+$100,x
        eor #$80
        sta screen+$100,x
        lda screen+$200,x
        eor #$80
        sta screen+$200,x
        lda screen+$300,x
        eor #$80
        sta screen+$300,x
        inx
        bne screenloop
        iny
        jsr delay // a small delay before the next flash
        cpy #$10 // has it flashed 10 times?
        bne bolt // if not restart the loop 
        rts // finish
delay:
        php // preserve the registers, push the status register to the stack
        pha // accumulator to the stack
        txa 
        pha // x reg now in acc goes to the stack
        tya
        pha // and the y register
        ldy #$00
        ldx #$00
!loop:
        iny
        bne !loop- // let the y register cycle from 0 to 255
        inx
        cpx #30
        bne !loop- // let the x register cycle all the above 30 times
        pla // pull y from the stack into the acc
        tay // put it into y
        pla
        tax // same for x
        pla // same for acc
        plp // same for process register
        rts

drawscreen:
        lda #<background // load 16 bit address for background into ZP
        sta $21
        lda #>background
        sta $22
        jsr copybgtoscreen
        rts

copybgtoscreen:
        lda #<screen // store start of screen mem into ZP too
        sta $1f
        lda #>screen 
        sta $20
        ldx #4 // mainloop will run 4 times
mainlp:
        ldy #$00
!loop:   // this loop will run 255 times
        lda ($21),y // load char from background memory
        sta ($1f),y // store in screen mem. both these are indirect indexed
        dey // decrease y
        bne !loop- // keep going until y is zero
        inc $20 // increase the high byte of screen address (+255)
        inc $22 // do the same to high byte of background address
        dex 
        bne mainlp// keep going until x is 0
        rts
        
waitforspace:
        jsr chrin // check keyboasrd input
        beq waitforspace // wait for a key to be pressed
        rts

waitforkey:
!loop:
        lda $97
        cmp #$ff
        beq !loop-
        rts

clearscreen:
        lda #$93
        jsr chrout // output the clr-screen (chr$(147) to the screen)     
        rts


background: // drawing of a house on the road made with screen designer.
        .byte    $20,$20,$20,$20,$20,$14,$08,$15,$0E,$04,$05,$12,$13,$14,$0F,$12,$0D,$20,$0F,$16,$05,$12,$20,$13,$03,$08,$09,$0E,$04,$0C,$05,$12,$20,$14,$0F,$17,$05,$12,$13,$20
        .byte    $20,$20,$20,$20,$20,$20,$20,$20,$E9,$E0,$E0,$E0,$DF,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$20,$E9,$E0,$E0,$E0,$E0,$E0,$DF,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$E9,$E0,$E0,$E0,$E0,$E0,$E0,$E0,$DF,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$E0,$70,$72,$6E,$E0,$70,$72,$6E,$E0,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$E0,$6B,$5B,$73,$E0,$6B,$5B,$73,$E0,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$E0,$6D,$71,$7D,$E0,$6D,$71,$7D,$E0,$20,$20,$20,$20,$20,$20,$20,$20,$55,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$49,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$E0,$E0,$E0,$E0,$E0,$E0,$E0,$E0,$E0,$20,$20,$20,$20,$20,$20,$20,$20,$42,$90,$92,$8F,$93,$90,$85,$83,$94,$A0,$8C,$81,$8E,$85,$5D,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$E0,$70,$72,$6E,$E0,$E0,$66,$66,$E0,$20,$20,$20,$20,$20,$20,$20,$20,$4A,$40,$40,$40,$40,$40,$72,$72,$40,$40,$40,$40,$40,$40,$4B,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$E0,$6B,$5B,$73,$E0,$B3,$51,$66,$E0,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$5D,$5D,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$E0,$6D,$71,$7D,$E0,$E0,$66,$66,$E0,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$5D,$5D,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78,$78
        .byte    $20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64,$64
        .byte    $40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40
        .byte    $40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40
        .byte    $20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $E0,$E0,$E0,$E0,$E0,$20,$20,$E0,$E0,$E0,$E0,$E0,$20,$20,$E0,$E0,$E0,$E0,$E0,$20,$20,$E0,$E0,$E0,$E0,$E0,$20,$20,$E0,$E0,$E0,$E0,$E0,$20,$20,$E0,$E0,$E0,$E0,$E0
        .byte    $60,$60,$60,$60,$60,$60,$60,$60,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
        .byte    $20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20

sprbolt: // this is a table that alternates between an offset from the screen address
// and the charcter.  with each loop, the screen position in ZP gets updated
        .byte 60,$4e,39,$4e,40,$65,39,$4e,1,$65,39,$65,1,$65,39,$65,39,$4e
        .byte $00 //end of sprite

sprdrone: // sprite for drone
        .byte 40,$58,2,$58,38,$e2,1,$72,1,$e2
        .byte 0

drawdrone:
        lda #<screen
        sta $1f
        lda #>screen
        sta $20
        clc
        lda dronex
        adc $1f
        sta $1f
        lda #00
        adc $20
        sta $20
        clc
        ldx #0
        ldy #0
!loop:
        clc
        lda sprdrone,x
        beq !end+
        adc $1f
        sta $1f
        lda #00
        adc $20
        sta $20
        inx
        lda sprdrone,x
        sta ($1f),y
        inx
        jmp !loop-
!end:
        rts

removedrone:
        lda #<screen
        sta $1f
        lda #>screen
        sta $20
        lda #<background
        sta $21
        lda #>background
        sta $22
        clc
        lda dronex
        adc $1f
        sta $1f
        lda #00
        adc $20
        sta $20
        clc
        lda dronex
        adc $21
        sta $21
        lda #00
        adc $22
        sta $22
        ldx #00
        ldy #00
!loop:
        clc
        lda sprdrone,x
        beq !end+
        adc $1f
        sta $1f
        lda #00
        adc $20
        clc
        lda sprdrone,x
        adc $21
        sta $21
        lda #00
        adc $22
        sta $22
        lda ($21),y
        sta ($1f),y
        inx
        inx
        jmp !loop-
!end:
        rts
        
        
drawbolt:
        lda #<screen // put screen start high and low bytes into zp
        sta $1f
        lda #>screen
        sta $20
        ldx #0
        ldy #0
!loop:
        clc // clear the carry
        lda sprbolt,x // get the offset from screen ram
        beq !end+ // finish if it's zero
        adc $1f // add the offset to screen low byte with carry in case it exceeds 255
        sta $1f // store the low byte addition to low byte screen pos
        lda #00 
        adc $20 // add zero into the high byte plus the carry bit if 255 was exceeded
        sta $20
        inx // move to the screen character byte
        lda sprbolt,x // load the character code into acc
        sta ($1f),y // save it into the new screen pos we calculated
        inx // move to the next coord
        jmp !loop- // keep looping
!end:
        rts

removebolt: // same as drawbolt but puts a space into screen mem instead of the defined character
        lda #<screen
        sta $21
        lda #>screen
        sta $22
        ldx #0
        ldy #0
!loop:
        clc
        lda sprbolt,x
        beq !end+
        adc $21
        sta $21
        lda #00
        adc $22
        sta $22
        inx
        lda #$20
        sta ($21),y
        inx
        jmp !loop-
!end:
        rts

dronex:
        .byte $00
keycountdown:     
        .byte $00
